/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.marcos;

/**
 *
 * @author MarcosGalvez
 */
public class Pizzeria {
    public static void main(String[] args) {
        //03 instanciar objetos
        Pizza pizza1 = new Pizza();
        pizza1.getNombre();
        System.out.println(pizza1.getNombre());
        pizza1.setNombre("Espanola");
        System.out.println("nombre pizza1:" + pizza1.getNombre());
        
        
        Pizza pizza2 = new Pizza("PEPERONI","FAMILIAR","NORMAL");
        System.out.println("pizza2: " + pizza2.toString()); 
        pizza2.setNombre("vegetariana");
        System.out.println(pizza2.toString());
        
        
        Pizza pizza3 = new Pizza ("CHILENA","MEDIANA","NORMAL");
        pizza3.preparar();
        pizza3.calentar();
         pizza3.getNombre();
        System.out.println(pizza3.getNombre());
        System.out.println("tamaño pizza3:" + pizza3.getTamano());
        System.out.println("masa pizza3" + pizza3.getMasa());
       
        
       
               
    }
    
  
    
}
