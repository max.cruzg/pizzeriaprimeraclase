/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.marcos;

/**
 *
 * @author MarcosGalvez
 */
public class Pizza {
    //01 crear atributos
    private String nombre,tamano,masa;
    
    //02 construir metodos (customer)
    public void preparar(){
        System.out.println("estamos preparando tu pizza");
        
    }
    public void calentar(){
        System.out.println("estamos calentando tu pizza");
    }
    // 04 crear costructor con y sin parametros

    public Pizza() {
    }

    public Pizza(String nombre, String tamano, String masa) {
        this.nombre = nombre;
        this.tamano = tamano;
        this.masa = masa;
    }
    
    // 05 crear metodos setter y getter

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getMasa() {
        return masa;
    }

    public void setMasa(String masa) {
        this.masa = masa;
    }
    //06 CREAR METODO toString

    @Override
    public String toString() {
        return "Pizza{" + "nombre=" + nombre + ", tamano=" + tamano + ", masa=" + masa + '}';
    }
    
    
}
